'use strict';

/**
 * @ngdoc function
 * @name fashnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fashnApp
 */
angular.module('fashnApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
