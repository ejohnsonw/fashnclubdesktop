/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      Fashn
Template: /yeoman-angular/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:ImageAddCtrl
 * @description
 * # ImageAddCtrl
 * Controller of the Fashn
 */
angular.module('Fashn.controllers')
    .controller('ImageAddCtrl', function($scope, ImageWS, ImageCollectionWS) {
        $scope.image = {};
        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/images"
                // false String String description
                // false Long String id
                // false ImageCollection String imageCollection
                // false String String path
                // false String String title
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            ImageWS.create($scope.successcb, $scope.errorcb, $scope.image);
        }

        $scope.cancel = function() {
            document.location.href = "#/images"
        }

        // false String String description
        // false Long String id
        // false ImageCollection String imageCollection
        // false String String path
        // false String String title

        $scope.loadReferences = function() {
            // false String String description
            // false Long String id
            // false ImageCollection String imageCollection
            ImageCollectionWS.list(function(data) {
                    $scope.imageCollections = data;
                }, $scope.errorcb)
                // false String String path
                // false String String title

        }
        $scope.loadReferences();


    }).config(function($routeProvider) {
        $routeProvider
            .when('/addImage', {
                templateUrl: 'views/imageForm.html',
                controller: 'ImageAddCtrl'
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 21.49 minutes to type the 2149+ characters in this file.
 */