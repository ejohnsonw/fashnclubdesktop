/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     AssetAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      Fashn
Template: /yeoman-angular/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:AssetAddCtrl
 * @description
 * # AssetAddCtrl
 * Controller of the Fashn
 */
angular.module('Fashn.controllers')
    .controller('AssetAddCtrl', function($scope, ImageWS, AssetWS, CompositionWS) {
        $scope.asset = {};
        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/assets"
                // false Composition String composition
                // false Long String id
                // false Image String image
                // false Float String x
                // false Float String y
                // false Float String z
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            AssetWS.create($scope.successcb, $scope.errorcb, $scope.asset);
        }

        $scope.cancel = function() {
            document.location.href = "#/assets"
        }

        // false Composition String composition
        // false Long String id
        // false Image String image
        // false Float String x
        // false Float String y
        // false Float String z

        $scope.loadReferences = function() {
            // false Composition String composition
            CompositionWS.list(function(data) {
                    $scope.compositions = data;
                }, $scope.errorcb)
                // false Long String id
                // false Image String image
            ImageWS.list(function(data) {
                    $scope.images = data;
                }, $scope.errorcb)
                // false Float String x
                // false Float String y
                // false Float String z

        }
        $scope.loadReferences();


    }).config(function($routeProvider) {
        $routeProvider
            .when('/addAsset', {
                templateUrl: 'views/assetForm.html',
                controller: 'AssetAddCtrl'
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 23.09 minutes to type the 2309+ characters in this file.
 */