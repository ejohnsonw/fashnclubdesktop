/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     UserEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      Fashn
Template: /yeoman-angular/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * 
 */
angular.module('Fashn.controllers')
    .controller('UserEditCtrl', function($scope, $routeParams, $rootScope, UserWS, ImageCollectionWS, CompositionWS) {
        $scope.user = {};
        $scope.edit = true;
        $scope.save = function() {
            UserWS.update($scope.updatesuccesscb, $scope.errorcb, $scope.user);
        }

        $scope.delete = function() {
            UserWS.delete($scope.updatesuccesscb, $scope.errorcb, $scope.user);
        }
        $scope.cancel = function() {
            document.location.href = "#/users"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/users"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.user = data;
            // true Collection Composition compositions
            if (typeof $scope.user.compositions == "undefined") {
                $scope.user.compositions = [];
            }

            // false String String email
            // false Long String id
            // true Collection ImageCollection imageCollections
            if (typeof $scope.user.imageCollections == "undefined") {
                $scope.user.imageCollections = [];
            }

            // false String String name
            // false String String photo
        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }

        // true Collection Composition compositions
        if (typeof $scope.user.compositions == "undefined") {
            $scope.user.compositions = [];
        }

        $scope.selectedComposition = function(compositions, isCheck) {
                var idx = findIndexId($scope.user.compositions, compositions.id)

                if (idx == -1) {
                    if (typeof isCheck == "undefined") {
                        $scope.user.compositions.push(compositions);
                        return false;
                    } else {
                        return false;
                    }
                } else {
                    if (typeof isCheck == "undefined") {
                        $scope.user.compositions.splice(idx, 1);
                        return true;
                    } else {
                        return true;
                    }
                }

            }
            // false String String email
            // false Long String id
            // true Collection ImageCollection imageCollections
        if (typeof $scope.user.imageCollections == "undefined") {
            $scope.user.imageCollections = [];
        }

        $scope.selectedImageCollection = function(imageCollections, isCheck) {
                var idx = findIndexId($scope.user.imageCollections, imageCollections.id)

                if (idx == -1) {
                    if (typeof isCheck == "undefined") {
                        $scope.user.imageCollections.push(imageCollections);
                        return false;
                    } else {
                        return false;
                    }
                } else {
                    if (typeof isCheck == "undefined") {
                        $scope.user.imageCollections.splice(idx, 1);
                        return true;
                    } else {
                        return true;
                    }
                }

            }
            // false String String name
            // false String String photo

        $scope.loadReferences = function() {

            // true Collection Composition compositions
            CompositionWS.list(function(data) {
                    $scope.compositionss = data;
                }, $scope.errorcb)
                // false String String email
                // false Long String id
                // true Collection ImageCollection imageCollections
            ImageCollectionWS.list(function(data) {
                    $scope.imageCollectionss = data;
                }, $scope.errorcb)
                // false String String name
                // false String String photo

        }
        $scope.loadReferences();
        UserWS.retrieve($scope.successcb, $scope.errorcb, $routeParams.id);
    }).config(function($routeProvider) {
        $routeProvider
            .when('/user/:id', {
                templateUrl: 'views/userForm.html',
                controller: 'UserEditCtrl'
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 43.33 minutes to type the 4333+ characters in this file.
 */