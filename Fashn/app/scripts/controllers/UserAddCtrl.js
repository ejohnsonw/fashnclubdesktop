/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     UserAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      Fashn
Template: /yeoman-angular/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:UserAddCtrl
 * @description
 * # UserAddCtrl
 * Controller of the Fashn
 */
angular.module('Fashn.controllers')
    .controller('UserAddCtrl', function($scope, UserWS, ImageCollectionWS, CompositionWS) {
        $scope.user = {};
        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/users"
                // true Collection Composition compositions
            if (typeof $scope.user.compositions == "undefined") {
                $scope.user.compositions = [];
            }
            // false String String email
            // false Long String id
            // true Collection ImageCollection imageCollections
            if (typeof $scope.user.imageCollections == "undefined") {
                $scope.user.imageCollections = [];
            }
            // false String String name
            // false String String photo
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            UserWS.create($scope.successcb, $scope.errorcb, $scope.user);
        }

        $scope.cancel = function() {
            document.location.href = "#/users"
        }

        // true Collection Composition compositions
        $scope.user.compositions = [];
        $scope.selectedComposition = function(compositions) {
            var idx = $scope.user.compositions.indexOf(compositions)
            if (idx == -1) {
                $scope.user.compositions.push(compositions);
            } else {
                $scope.user.compositions.splice(idx, 1);
            }
            console.log($scope.user.compositions)
        }

        // false String String email
        // false Long String id
        // true Collection ImageCollection imageCollections
        $scope.user.imageCollections = [];
        $scope.selectedImageCollection = function(imageCollections) {
            var idx = $scope.user.imageCollections.indexOf(imageCollections)
            if (idx == -1) {
                $scope.user.imageCollections.push(imageCollections);
            } else {
                $scope.user.imageCollections.splice(idx, 1);
            }
            console.log($scope.user.imageCollections)
        }

        // false String String name
        // false String String photo

        $scope.loadReferences = function() {
            // true Collection Composition compositions
            CompositionWS.list(function(data) {
                    $scope.compositionss = data;
                }, $scope.errorcb)
                // false String String email
                // false Long String id
                // true Collection ImageCollection imageCollections
            ImageCollectionWS.list(function(data) {
                    $scope.imageCollectionss = data;
                }, $scope.errorcb)
                // false String String name
                // false String String photo

        }
        $scope.loadReferences();


    }).config(function($routeProvider) {
        $routeProvider
            .when('/addUser', {
                templateUrl: 'views/userForm.html',
                controller: 'UserAddCtrl'
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 35.07 minutes to type the 3507+ characters in this file.
 */