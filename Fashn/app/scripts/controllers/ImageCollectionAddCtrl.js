/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageCollectionAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      Fashn
Template: /yeoman-angular/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:ImageCollectionAddCtrl
 * @description
 * # ImageCollectionAddCtrl
 * Controller of the Fashn
 */
angular.module('Fashn.controllers')
    .controller('ImageCollectionAddCtrl', function($scope, UserWS, ImageWS, ImageCollectionWS) {
        $scope.imageCollection = {};
        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/imageCollections"
                // false Long String id
                // true Collection Image images
            if (typeof $scope.imageCollection.images == "undefined") {
                $scope.imageCollection.images = [];
            }
            // false String String name
            // false User String user
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            ImageCollectionWS.create($scope.successcb, $scope.errorcb, $scope.imageCollection);
        }

        $scope.cancel = function() {
            document.location.href = "#/imageCollections"
        }

        // false Long String id
        // true Collection Image images
        $scope.imageCollection.images = [];
        $scope.selectedImage = function(images) {
            var idx = $scope.imageCollection.images.indexOf(images)
            if (idx == -1) {
                $scope.imageCollection.images.push(images);
            } else {
                $scope.imageCollection.images.splice(idx, 1);
            }
            console.log($scope.imageCollection.images)
        }

        // false String String name
        // false User String user

        $scope.loadReferences = function() {
            // false Long String id
            // true Collection Image images
            ImageWS.list(function(data) {
                    $scope.imagess = data;
                }, $scope.errorcb)
                // false String String name
                // false User String user
            UserWS.list(function(data) {
                $scope.users = data;
            }, $scope.errorcb)

        }
        $scope.loadReferences();


    }).config(function($routeProvider) {
        $routeProvider
            .when('/addImageCollection', {
                templateUrl: 'views/imageCollectionForm.html',
                controller: 'ImageCollectionAddCtrl'
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 26.44 minutes to type the 2644+ characters in this file.
 */