/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     CompositionEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      Fashn
Template: /yeoman-angular/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:CompositionsCtrl
 * @description
 * # CompositionsCtrl
 * 
 */
angular.module('Fashn.controllers')
    .controller('CompositionEditCtrl', function($scope, $routeParams, $rootScope, UserWS, AssetWS, CompositionWS) {
        $scope.composition = {};
        $scope.edit = true;
        $scope.save = function() {
            CompositionWS.update($scope.updatesuccesscb, $scope.errorcb, $scope.composition);
        }

        $scope.delete = function() {
            CompositionWS.delete($scope.updatesuccesscb, $scope.errorcb, $scope.composition);
        }
        $scope.cancel = function() {
            document.location.href = "#/compositions"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/compositions"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.composition = data;
            // true Collection Asset assets
            if (typeof $scope.composition.assets == "undefined") {
                $scope.composition.assets = [];
            }

            // false Long String id
            // false String String name
            // false User String user
        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }

        // true Collection Asset assets
        if (typeof $scope.composition.assets == "undefined") {
            $scope.composition.assets = [];
        }

        $scope.selectedAsset = function(assets, isCheck) {
                var idx = findIndexId($scope.composition.assets, assets.id)

                if (idx == -1) {
                    if (typeof isCheck == "undefined") {
                        $scope.composition.assets.push(assets);
                        return false;
                    } else {
                        return false;
                    }
                } else {
                    if (typeof isCheck == "undefined") {
                        $scope.composition.assets.splice(idx, 1);
                        return true;
                    } else {
                        return true;
                    }
                }

            }
            // false Long String id
            // false String String name
            // false User String user

        $scope.loadReferences = function() {

            // true Collection Asset assets
            AssetWS.list(function(data) {
                    $scope.assetss = data;
                }, $scope.errorcb)
                // false Long String id
                // false String String name
                // false User String user
            UserWS.list(function(data) {
                $scope.users = data;
            }, $scope.errorcb)

        }
        $scope.loadReferences();
        CompositionWS.retrieve($scope.successcb, $scope.errorcb, $routeParams.id);
    }).config(function($routeProvider) {
        $routeProvider
            .when('/composition/:id', {
                templateUrl: 'views/compositionForm.html',
                controller: 'CompositionEditCtrl'
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 30.849998 minutes to type the 3085+ characters in this file.
 */