'use strict';

/**
 * @ngdoc function
 * @name fashnApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the fashnApp
 */
angular.module('fashnApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
