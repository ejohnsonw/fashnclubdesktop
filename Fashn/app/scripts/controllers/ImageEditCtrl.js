/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      Fashn
Template: /yeoman-angular/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:ImagesCtrl
 * @description
 * # ImagesCtrl
 * 
 */
angular.module('Fashn.controllers')
    .controller('ImageEditCtrl', function($scope, $routeParams, $rootScope, ImageWS, ImageCollectionWS) {
        $scope.image = {};
        $scope.edit = true;
        $scope.save = function() {
            ImageWS.update($scope.updatesuccesscb, $scope.errorcb, $scope.image);
        }

        $scope.delete = function() {
            ImageWS.delete($scope.updatesuccesscb, $scope.errorcb, $scope.image);
        }
        $scope.cancel = function() {
            document.location.href = "#/images"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/images"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.image = data;
            // false String String description
            // false Long String id
            // false ImageCollection String imageCollection
            // false String String path
            // false String String title
        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }

        // false String String description
        // false Long String id
        // false ImageCollection String imageCollection
        // false String String path
        // false String String title

        $scope.loadReferences = function() {

            // false String String description
            // false Long String id
            // false ImageCollection String imageCollection
            ImageCollectionWS.list(function(data) {
                    $scope.imageCollections = data;
                }, $scope.errorcb)
                // false String String path
                // false String String title

        }
        $scope.loadReferences();
        ImageWS.retrieve($scope.successcb, $scope.errorcb, $routeParams.id);
    }).config(function($routeProvider) {
        $routeProvider
            .when('/image/:id', {
                templateUrl: 'views/imageForm.html',
                controller: 'ImageEditCtrl'
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 23.05 minutes to type the 2305+ characters in this file.
 */