var Sails = require('sails'),
    sails;



before(function(done) {
    this.timeout(0);
    Sails.lift({
        // configuration for testing purposes

    }, function(err, server) {
        sails = server;
        if (err) return done(err);
        // here you can load fixtures, etc.
        console.log("lifted");
        
        var Barrels = require('barrels');
        var barrels = new Barrels();
        var fixtures = barrels.data;
        console.log(fixtures);
        

        barrels.populate(function(err) {
            if (err) {
                return done(err); // Higher level callback
            }

            //done();
            done(err, sails);
        });



        
    });
});

after(function(done) {
    // here you can clear fixtures, etc.
    console.log("lowered");
    sails.lower(done);
});
